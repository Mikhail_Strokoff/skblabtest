/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebTests;

import Pages.CellListPage;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * @author Strokoff Mikhail
 */
public class JUnitTest {

    private Logger logger;
    private RemoteWebDriver webDriver;
    private final String startUrl = "http://samples.gwtproject.org/samples/Showcase/Showcase.html#!CwCellList";

    private CellListPage cellListPage;

    public JUnitTest() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "./src/ChromeDriver/chromedriver.exe");
        logger = Logger.getLogger(JUnitTest.class.getName());
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        webDriver.get(startUrl);
        cellListPage = new CellListPage(webDriver);
    }

    @After
    public void tearDown() {
        webDriver.quit();
    }

    @Test
    public void cellListPaginationTest() {
        int totalCount = 250;
        logger.log(Level.INFO, "total count = {0}", totalCount);

        for (int count = 30; count <= totalCount; count += 20) {
            logger.log(Level.INFO, "current count = {0}", count);
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
            assertTrue("Unexpected cell count ! ", cellListPage.checkListCount(count, totalCount));
            cellListPage.cellList.sendKeys(Keys.END.toString());
        }
    }

    @Test
    public void cellListCreateContactTest() {
        int totalCount = 250;
        while (!cellListPage.checkListCount(totalCount, totalCount)) {
            cellListPage.cellList.sendKeys(Keys.END.toString());
        }
        cellListPage.cellForm.setContact("Mike", "Smirnov", "09.12.1994", "Drutensky avenu, 12");
        cellListPage.cellForm.createButton.click();
        totalCount++;
        cellListPage.cellList.get(totalCount-2).click();
        cellListPage.cellList.get(totalCount-1).click();
        cellListPage.cellForm.checkContact("Mike", "Smirnov", "September 12, 1994", "Drutensky avenu, 12");
    }

    @Test
    public void cellListUpdateContactTest() {
        cellListPage.cellList.get(0).click();
        cellListPage.cellForm.setContact("Mike", "Smirnov", "09.12.1994", "Drutensky avenu, 12");
        cellListPage.cellForm.updateButton.click();
        cellListPage.cellList.get(1).click();
        cellListPage.cellList.get(0).click();
        cellListPage.cellForm.checkContact("Mike", "Smirnov", "September 12, 1994", "Drutensky avenu, 12");
    }

    @Test
    public void cellListGenerate50ContactsTest() {
        int count = 30;
        int totalCount = 250;
        logger.log(Level.INFO, "total count = {0}", totalCount);
        assertTrue("Unexpected cell count ! ", cellListPage.checkListCount(count, totalCount));
        cellListPage.generateButton.click();
        totalCount += 50;
        logger.log(Level.INFO, "total count = {0}", totalCount);
        assertTrue("Unexpected cell count ! ", cellListPage.checkListCount(count, totalCount));
        cellListPage.generateButton.click();
        totalCount += 50;
        logger.log(Level.INFO, "total count = {0}", totalCount);
        assertTrue("Unexpected cell count ! ", cellListPage.checkListCount(count, totalCount));
        cellListPage.generateButton.click();
        totalCount += 50;
        logger.log(Level.INFO, "total count = {0}", totalCount);
        assertTrue("Unexpected cell count ! ", cellListPage.checkListCount(count, totalCount));
        cellListPage.generateButton.click();
        totalCount += 50;
        logger.log(Level.INFO, "total count = {0}", totalCount);
        assertTrue("Unexpected cell count ! ", cellListPage.checkListCount(count, totalCount));
    }
}
