/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Strokoff Mikhail
 */
public class SkbCellList {

    private final By ListBy;
    private final SkbElement ParentElement;
    private SkbElement CellListCount;

    public SkbCellList(String xpathString, WebDriver driver) {
        this.ListBy = By.xpath(xpathString);
        ParentElement = new SkbElement(ListBy, driver);
        CellListCount = new SkbElement(By.xpath(xpathString.concat("/following-sibling::*[1]")), driver);
    }

    public String cellListCountText() {
        return CellListCount.WaitReadyElement().getText();
    }

    public WebElement get(int index) {
        waitCountElements(index + 1, 15000);
        return ParentElement.WaitReadyElement().findElements(By.tagName("table")).get(index);
    }

    public String getName(int index) {
        return get(index).findElements(By.tagName("td")).get(1).getText();
    }

    public String getAddress(int index) {
        return get(index).findElements(By.tagName("td")).get(2).getText();
    }

    public void click(int index) {
        get(index).click();
    }

    public void sendKeys(String text) {
        ParentElement.WaitReadyElement().sendKeys(text);
    }

    public void waitCountElements(int count, int timeout) {
        int timeoutPart = timeout / 5;

        for (int i = 0; i <= 5; i++) {
            try {
                if (i > 0) {
                    Thread.sleep(timeoutPart); // первая итерация без ожидания
                }
                int size = ParentElement.WaitReadyElement().findElements(By.tagName("table")).size();
                if (size >= count) {
                    return;
                }
                sendKeys(Keys.UP.toString() + Keys.END);
            } catch (InterruptedException ex) {
            }
        }
    }
}
