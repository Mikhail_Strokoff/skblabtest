/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Elements;

import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Strokoff Mikhail
 */
public class SkbCellForm {

    private final By ListBy;
    private final SkbElement ParentElement;

    public WebElement firstName;
    public WebElement lastName;
    public WebElement birthday;
    public WebElement address;
    public WebElement updateButton;
    public WebElement createButton;

    public SkbCellForm(String xpathString, WebDriver driver) {
        this.ListBy = By.xpath(xpathString);
        ParentElement = new SkbElement(ListBy, driver);

        firstName = ParentElement.WaitReadyElement().findElements(By.tagName("input")).get(0);
        lastName = ParentElement.WaitReadyElement().findElements(By.tagName("input")).get(1);
        birthday = ParentElement.WaitReadyElement().findElements(By.tagName("input")).get(2);
        address = ParentElement.WaitReadyElement().findElement(By.tagName("textarea"));
        updateButton = driver.findElement(By.xpath(xpathString.concat("//button[ . = 'Update Contact']")));
        createButton = driver.findElement(By.xpath(xpathString.concat("//button[ . = 'Create Contact']")));
    }

    public void setContact(String firstName, String lastName, String birthday, String address) {
        this.firstName.clear();
        this.firstName.sendKeys(firstName + Keys.TAB);
        this.lastName.clear();
        this.lastName.sendKeys(lastName + Keys.TAB);
        this.birthday.clear();
        this.birthday.sendKeys(birthday + Keys.TAB);
        this.address.clear();
        this.address.sendKeys(address + Keys.TAB);
    }

    public void checkContact(String firstName, String lastName, String birthday, String address) {
        assertEquals("First name is unexpected ! ", firstName, this.firstName.getAttribute("value"));
        assertEquals("Last name is unexpected ! ", lastName, this.lastName.getAttribute("value"));
        assertEquals("Birthday is unexpected ! ", birthday, this.birthday.getAttribute("value"));
        assertEquals("Address is unexpected ! ", address, this.address.getAttribute("value"));
    }
}
