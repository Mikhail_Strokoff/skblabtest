/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pages;

import Elements.SkbCellForm;
import Elements.SkbCellList;
import Elements.SkbElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Strokoff Mikhail
 */
public class CellListPage {

    public SkbCellList cellList;
    public SkbCellForm cellForm;
    public WebElement generateButton;
    private final String cellListXpath = "//*[@class='GNHGC04CJJ']";
    private final String formXpath = "//*[@class='GNHGC04CIJ']";

    public CellListPage(WebDriver driver) {
        cellList = new SkbCellList(cellListXpath, driver);
        cellForm = new SkbCellForm(formXpath, driver);
        generateButton = new SkbElement(By.xpath(formXpath.concat("/button")), driver).WaitReadyElement();
    }

    public boolean checkListCount(int displayCount, int totalCount) {
        return cellList.cellListCountText()
                .equals("0 - "
                        .concat(String.valueOf(displayCount))
                        .concat(" : ")
                        .concat(String.valueOf(totalCount))
                );
    }
}
